package tv.snews.util {

import flash.display.DisplayObject;
import flash.display.MovieClip;
import flash.display.Shape;
import flash.display.Sprite;
import flash.events.Event;
import flash.text.TextField;
import flash.text.TextFieldAutoSize;

import tv.snews.sgs.SGSTemplate;

public class Swap extends flash.display.MovieClip {

	private var tfName:String;
	private var movieName:String;
	private var template:MovieClip;

	function Swap(movie:MovieClip, tf:TextField, template:MovieClip) {
		this.movieName = movie.name;
		this.tfName = tf.name;
		this.template = template;
	}

	public function start():void {
		addEventListener(Event.ENTER_FRAME, doSwap);
	}

	private function doSwap(e:Event):void {
		if (template[movieName] && template[tfName]) {
			var text:String = template[tfName].text;
			if(text) text = text.toLowerCase();
			
			try{
                template[movieName].gotoAndStop(text);
			}catch(e:Object ) {
                template[movieName].gotoAndStop(1);
            }

			
			removeEventListener(Event.ENTER_FRAME, doSwap);
		}
	}

}

}