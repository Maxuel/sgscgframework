package tv.snews.util {

import flash.display.DisplayObject;
import flash.display.Shape;
import flash.display.Sprite;
import flash.events.Event;
import flash.text.TextField;
import flash.text.TextFieldAutoSize;

import tv.snews.sgs.SGSTemplate;

public class Crawler extends flash.display.MovieClip {

	private var speed:Number;
	private var tfName:String;
	private var originalX:Number;
	private var orgWidth:Number;
	private var template:SGSTemplate;
	private var isInitialized:Boolean;
	private var timeHolder:String;
	private var boundsName:String;

	function Crawler(template:SGSTemplate, speed:TextField,  bounds:DisplayObject, tf:TextField) {
		this.template = template;
		this.tfName = tf != null ? tf.name : bounds.name;
		this.boundsName = bounds.name;

		if (speed is TextField) {
			timeHolder = speed.name;
		} else if (speed is Number) {
			this.speed = Number(speed);
		} else {
			throw new Error("Um número ou campo de texto contendo um número deve ser passado no primeiro parâmetro.")
		}
	}

	public function start():void {
		addEventListener(Event.ENTER_FRAME, scrollText);
	}

	public function scrollText(e:Event):void {
		if (!isInitialized) {
			initialize();
		} else {
			//Se o elemento existir
			if (template[tfName]) {
				template[tfName].x -= speed;
				if (template[tfName].x <= -template[tfName].width) {
					template[tfName].x = this.orgWidth;
				}
			}
		}
	}

	public function initialize():void {
		if (!template[tfName] || (timeHolder && !template[timeHolder]))
			return;

		var tilted:Sprite = new Sprite();
		tilted.cacheAsBitmap = true;
		addChild(tilted);

		this.orgWidth = Math.round(template[boundsName].width);
		var mask:Shape = new Shape();
		drawGraphics(mask, template[boundsName]);
		tilted.mask = mask;

		tilted.x = originalX = template[boundsName].x;
		TextField(template[tfName]).autoSize = TextFieldAutoSize.LEFT;
		TextField(template[tfName]).wordWrap = false;
		TextField(template[tfName]).cacheAsBitmap = true;

		tilted.addChild(template[tfName]);
		template[tfName].x = this.orgWidth;
		template[tfName].y = template[boundsName].y;

		if (timeHolder)
			speed = Number(template[timeHolder].text);
		speed = Math.round(speed);
		isInitialized = true;
	}

	//---------------------------
	//  Helper's
	//---------------------------

	public function drawGraphics(shape:Shape, displayObject:DisplayObject):void {
		shape.graphics.clear();
		shape.graphics.beginFill(0x0);
		shape.graphics.drawRect(displayObject.x, displayObject.y, displayObject.width, displayObject.height);
		shape.graphics.endFill();
	}

}

}