package tv.snews.util {

import flash.events.TimerEvent;
import flash.net.URLLoader;
import flash.text.TextField;
import flash.net.URLRequest

import flash.display.DisplayObject;
import flash.events.Event;
import flash.utils.Timer;

import flashx.textLayout.formats.Float;


import tv.snews.sgs.SGSTemplate;


public class RSSFeed extends Crawler{
    private static var RSS_URL:String = "";
    private static var rssTextField:TextField;

    private static var updateTimer:Timer;
    private static var updateInterval:Number;
    private static var rssSeparator:TextField;

    public function RSSFeed(template:SGSTemplate,  bounds:DisplayObject, tf:TextField, rssUrl:TextField, speed:TextField = null, interval:TextField = null, separator:TextField = null) {
        RSS_URL = rssUrl.text;
        rssTextField = tf;

        if(tf == null) {
            rssTextField = new TextField()
            rssTextField.text = ""
        }
        if(speed == null) {
            speed = new TextField();
            speed.text = "1.0"
        }
        if(interval == null) {
            updateInterval = parseFloat("10");//Padrao 10 minutos
        }
        else{
            updateInterval = parseFloat(interval.text);

            if(updateInterval <= 0)
                updateInterval = 10;//Padrao 10 minutos
        }

        if(separator == null) {
            rssSeparator = new TextField();
            rssSeparator.text = "•"

        }

        trace("rssUrl: "+rssUrl.text)
        trace("speed: "+speed.text)
        trace("interval: "+updateInterval)
        trace("separator: "+rssSeparator.text)

        super(template, speed, bounds, rssTextField);

        updateTimer = new Timer(updateInterval*60000);
        updateTimer.addEventListener(TimerEvent.TIMER, timerListener);

        function timerListener (e:TimerEvent):void{
            LoadUrl()
        }

        LoadUrl();//execucao inicial

        updateTimer.start();
    }

    private function LoadUrl():void {
        var urlLoader:URLLoader = new URLLoader();
        trace("Loading...")
        urlLoader.addEventListener(Event.COMPLETE, loadCompleted);
        urlLoader.load(new URLRequest("http://localhost:3120/api/util/rss?url="+RSS_URL))

    }

    private function loadCompleted( event:Event ) : void {
        try {
            trace(event.target.data)
            if (event.target.data) {
                trace("Parsing data...")
                var res:Object = JSON.parse(event.target.data)

                if(res && res.error == false){
                    trace("data OK")
                    var rssText:String = "";
                    trace(res.data)
                    for each(var str:String in res.data) {
                        rssText += str + " " + rssSeparator.text + " ";
                    }
                    rssTextField.text = rssText;
                }
            }
        }
        catch (e:Object){}
    }
}

}
