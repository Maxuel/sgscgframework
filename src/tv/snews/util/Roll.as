package tv.snews.util {

import flash.display.DisplayObject;
import flash.display.Shape;
import flash.display.Sprite;
import flash.events.Event;
import flash.text.TextField;
import flash.text.TextFieldAutoSize;

import tv.snews.sgs.SGSTemplate;

public class Roll extends flash.display.MovieClip{

	private var elements:Array = new Array();
	private var time:Number;
	private var totalHeight:Number = 0;
	private var spacing:Number;
	private var stopAtLastItem:Boolean;
	private var boundsName:String;
	private var originalY:Number;
	private var orgHeight:Number;
	private var template:SGSTemplate;
	private var isInitialized:Boolean;
	private var speed:Number;
	private var timeHolder:String;

	function Roll(template:SGSTemplate, time:Object, spacing:Number, stopAtLastItem:Boolean, bounds:DisplayObject, elements:Array) {
		this.template = template;
		this.spacing = spacing;
		this.stopAtLastItem = stopAtLastItem;
		this.boundsName = bounds.name;
		this.totalHeight = stopAtLastItem ? 0 : bounds.height;
		this.elements = elements;

		if(time is TextField ){
			timeHolder = time.name;
		} else if(time is Number){
			this.time = Number(time);
		} else {
			throw new Error("Um número ou campo de texto contendo um número deve ser passado no primeiro parâmetro.")
		}
	}

	public function start():void {
		addEventListener(Event.ENTER_FRAME, scrollText);
	}

	private function scrollText(e:Event):void {
		if(!isInitialized){
			initialize();
		} else {
			for(var i:Number = 0; i < elements.length; i++){

				//Se o elemento existir
				if(template[elements[i]]){

					// Se for o primeiro da lista, deminuir y
					if(i == 0){
						this.template[elements[i]].y -= this.speed;
					}
					//Se não for, diminui o y de acordo com o item anterior
					else {
						//Se o elemento anterior existir
						if(template[this.elements[i - 1]]){
							this.template[elements[i]].y = this.template[this.elements[i - 1]].y + this.template[this.elements[i - 1]].height + this.spacing;
						}
						else{
							this.template[elements[i]].y -= speed;
						}
					}

					//Ultimo elemento
					if(i == elements.length-1 ){


						// Se é pra parar no ultimo item, o scroll deve parar quando o ultimo item estiver completamente visivel
						if(stopAtLastItem && template[elements[i]].y <= orgHeight - template[elements[i]].height){
							this.template[elements[i]].y = orgHeight - template[elements[i]].height;

							stopScrolling();
						}
						else if(this.template[elements[i]].y <=-1* (originalY + template[elements[i]].height)){
							stopScrolling();
						}
					}
				}

			}
		}
	}

	private function initialize():void {

		var elementNames:Array = new Array();

		for each (var elem:Object in elements ){
			if(!elem){
				return;
			} else if(!elem is DisplayObject){
				throw new Error("Todos os elementos devem ser instânciados antes da funcão ser chamada e ser ou herdar de flash.display.DisplayObject.");
			}
			elementNames.push(elem.name);
		}

		var tilted:Sprite = new Sprite();
		tilted.cacheAsBitmap = true;
		addChild(tilted);

		orgHeight = Math.round(template[boundsName].height);
		var mask:Shape = new Shape();
		drawGraphics(mask, template[boundsName]);
		tilted.mask = mask;

		tilted.y = originalY = template[boundsName].y;
		for (var i:Number = 0; i < elementNames.length; i++){
			if(template[elementNames[i]] is TextField){
				TextField(this.template[elementNames[i]]).autoSize = TextFieldAutoSize.CENTER;
				TextField(this.template[elementNames[i]]).cacheAsBitmap = true;
			}

			tilted.addChild(this.template[elementNames[i]]);
			//Move todos os elementos pra fora da tela
			template[elementNames[i]].y = this.orgHeight;
			this.totalHeight += template[elementNames[i]].height;
			if(i != 0)
				this.totalHeight += spacing;
		}

		elements = elementNames;

		var timeToComplete:Number = timeHolder ? Number(template[timeHolder].text) : time;
		this.speed = Math.round(this.totalHeight / (timeToComplete * template.stage.frameRate));
		isInitialized = true;
	}

	private function stopScrolling():void {
		this.removeEventListener(Event.ENTER_FRAME, scrollText);
	}

	//---------------------------
	//  Helper's
	//---------------------------

	private function drawGraphics(shape:Shape, displayObject:DisplayObject):void {
		shape.graphics.clear();
		shape.graphics.beginFill(0x0);
		shape.graphics.drawRect(displayObject.x, displayObject.y, displayObject.width, displayObject.height);
		shape.graphics.endFill();
	}

}

}