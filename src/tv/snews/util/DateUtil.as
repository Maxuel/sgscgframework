package tv.snews.util{
import mx.formatters.DateFormatter;


/**
	 * Objeto utilitário para trabalhar com datas.  Permite fazer as conversões
	 * de <b>Date</b> para <b>String</b> e vice-versa. São seguidos os padrões
	 * usados em DateFormatter.
	 *
	 * @see mx.formatters.DateFormatter
	 *
	 * @author Eliezer Reis
	 * @since 1.0
	 */
	public class DateUtil {
	
		private static const SECOND_VALUE:uint = 1000;
		private static const MINUTE_VALUE:uint = SECOND_VALUE * 60;
		private static const HOUR_VALUE:uint = MINUTE_VALUE * 60;
		private static const DAY_VALUE:uint = HOUR_VALUE * 24;

		//This is the total of milliseconds between UTC and LocalTime;
		private var _timeZone:Number = 0;

		public function DateUtil() {
			_timeZone = new Date().getTimezoneOffset() * MINUTE_VALUE;
		}
		
		/**
		 * @return Adquire a data corrente da máquina local sem as horas.
		 */
		public function timeZero():Date {
			var date:Date = new Date();
			date.setHours(0, 0, 0, 0);

			/*
			 * Quando for o primeiro dia do horário de verão, o Flex irá alterar a
			 * hora 00 para 01, já que a hora 00 não existe nesse dia. Nesse caso
			 * incrementa a data em um dia e corrige a hora.
			 */
			if (date.getHours() == 1) {
				date.setDate(date.getDate() + 1);
				date.setHours(0, 0, 0, 0);
			}

			return date;
		}
		
		public function get timeZone():Number {
			return _timeZone;
		}
		

		//--------------------------------------------------------------------------
		//
		//	Métodos Utilitários
		//
		//--------------------------------------------------------------------------
		
		/**
		 * Arredondamento de segundos de uma data, arredonda os milissegundos
		 * de uma data se acima de 500 milissegundos para cima se a baixo de 500
		 * para baixo.
		 * 
		 * @param date
		 * @return Date
		 */
		private function roundToSecond(date:Date):Date{
			if (date == null) {
				return null;
			}

			date.setSeconds(date.getSeconds() + date.getMilliseconds() / 500);
			date.setMilliseconds(0);

			return date;
		}
		
		/**
		 * Calcula o total de millisegundos da data levando em consideração somente
		 * os campos de hora (hora, minuto,segundo e millisegundo) ou seja, desprezando a data;
		 * 
		 * @param ms data que armazena as informações da hora.
		 * @return os milisegundos da hora informada.
		 **/
		public function getSecondsFromMilliseconds(ms:Number):Number {
			return ms / SECOND_VALUE;
		}

		/**
		 * Calcula o total de segundos da data levando em consideração somente
		 * os campos de hora (hora, minuto,segundo e millisegundo) ou seja, desprezando a data;
		 *
		 * @param time data que armazena as informações da hora.
		 * @return os milisegundos da hora informada.
		 **/
		public function getTime(time:Date):Number {
			if (time == null) {
				return 0;
			}

			//roundToSecond(time);

			var ms:Number = (time.getHours() * HOUR_VALUE) +
				(time.getMinutes() * MINUTE_VALUE) +
				(time.getSeconds() * SECOND_VALUE) +
				(time.getMilliseconds());

			return ms;
		}

		/**
		 * Calcula a diferença (distância) entre as horas das datas informadas.
		 * Esse calculo despreza qualquer campo de data (data,mes e ano);
		 * 
		 * @return milisegundos entre as duas datas;
		 */ 
		public function getTimeGap(time1:Date, time2:Date):Number {
			var ms1:Number = getTime(time1);
			var ms2:Number = getTime(time2);
			var diff:Number = ms1 > ms2 ? ms1 - ms2 : ms2 - ms1;
			
			return diff;
		}

		/**
		 * Calcula a diferença (distância) entre as horas das datas informadas.
		 * Esse calculo despreza qualquer campo de data (data,mes e ano);
		 *
		 * @return milisegundos entre as duas datas;
		 */
		public function getTimeExcess(time1:Date, time2:Date):Number {
			var ms1:Number = getTime(time1);
			var ms2:Number = getTime(time2);
			var diff:Number = ms1 + ms2;

			return diff;
		}

		/**
		 * Cria uma data no horário local passando os segundos da hora;
		 * @param sg Os segundos da data
		 * @return a data local;
		 */
		public function getTimeFromSeconds(sg:Number):Date {
			return  getTimeFromMilliseconds(sg * SECOND_VALUE);
		}

		/**
		 * Cria uma data no horário local passando os milisegundos da hora;
		 * @param ms Os milisegundos da data
		 * @return a data local;
		 */ 
		public function getTimeFromMilliseconds(ms:Number):Date {
			if (isNaN(ms)) {
				return timeZero();
			}

			var result:Date = timeZero();

			var hr:Number = Math.floor(ms / HOUR_VALUE);
			ms -= hr * HOUR_VALUE;

			var min:Number = Math.floor(ms / MINUTE_VALUE);
			ms -= min * MINUTE_VALUE;

			var sec:Number = Math.floor(ms / SECOND_VALUE);
			ms -= sec * SECOND_VALUE;
			
			result.setHours(hr, min, sec, ms);

			return result;
		}
		
		public function adjustTime(time:Date, timeZoneOffset:Number):Date {
			var ms:Number = getTime(time);
			return getTimeFromMilliseconds(ms + timeZoneOffset);
		}
		
		/**
		 * Soma as horas(horas,minutos, segundos e milisegundos) das datas informadas ;
		 * 
		 * @param time1 
		 * @param time2
		 * 
		 * @return nova data contendo o somatório das horas
		 */ 
		public function sumTimes(... args):Date {
			var ms:Number =  getTime(timeZero());
			for each (var time:Date in args) {
				ms = ms + getTime(time);
			}
			return getTimeFromMilliseconds(ms);
		}
		
		public function subtractTimes(time1:Date, time2:Date):Date {
			var ms:Number = getTimeGap(time1,time2);
			return getTimeFromMilliseconds(ms);
		}

		public function addTimes(time1:Date, time2:Date):Date {
			var ms:Number = getTimeExcess(time1,time2);
			return getTimeFromMilliseconds(ms);
		}
		
		/**
		 * Retorna uma nova data subtraindo os dias conforme a quantidade passada por
		 * daysToSubtrac.  
		 * 
		 * @param date Data para ser subtraída
		 * @param daysToSubtrac quantidade dias para subtrair
		 * 	
		 * @return data com os dias subtraídos.
		 */
        public function subtractDays(date:Date, daysToSubtrac:Number):Date {
            var time:Number = date.getTime() - (daysToSubtrac * DAY_VALUE);
            return new Date(time);
        }


		/**
		 * Retorna uma nova data somando as horas conforme a quantidade passada por
		 * hoursToSum.
		 *
		 * @param date Data para ser somada
		 * @param hoursToSum quantidade horas para somar
		 *
		 * @return data com as horas somadas.
		 */
		public function sumHours(date:Date, hoursToSum:Number):Date {
			var time:Number = date.getTime() + (hoursToSum * HOUR_VALUE);
			return new Date(time);
		}

		/**
		 * Faz a comparação entre duas datas, ignorando os valores de tempo.
		 * 
		 * @param date1
		 * @param date2
		 * @return Um inteiro que será positivo se a primeira data for a maior, 
		 * 		negativo se a segunda for a maior ou zero se ambas forem iguais. 
		 */
		public function compareDates(date1:Date, date2:Date):int {
			var sum1:Number = (date1.fullYear * 365) + (date1.month * 31) + date1.date;
			var sum2:Number = (date2.fullYear * 365) + (date2.month * 31) + date2.date;
			return sum1 - sum2;
		}
		
		/**
		 * Verifica se dois objetos Date são iguais, tratando a possibilidade de 
		 * qualquer um dos dois serem nulos.
		 * 
		 * <p>Caso os dois sejam nulos, eles serão considerados como iguais.</p>
		 * 
		 * @param valueA primeiro valor
		 * @param valueB segundo valor
		 * @return true caso os dois representem o mesmo valor de data e hora
		 */
		public function equals(valueA:Date, valueB:Date):Boolean {
			if (valueA == valueB) {
				return true;
			}
			if (valueA == null || valueB == null) {
				return false;
			}
			return valueA.getTime() == valueB.getTime();
		}

		public function format(date:Date, format:String):String {
			var df:DateFormatter = new DateFormatter();
			df.formatString=format;
			return df.format(date);
		}

	//--------------------------------------------------------------------------
	//
	//	String para Date
	//
	/**
	 * Faz a conversão de um <b>String</b> para <b>Date</b> no formato local de acordo com o formato de saída.
	 * Se a <b>String</b> ou o formato forem inválidos o retorno será <b>nulo</b>.
	 * <br/>
	 * Valores suportados pela expecificação de DateFormatter:<br/>
	 * <ul>
	 * 		<li>YY,YYYY,YYYYY {1970 ou superior} para anos;</li>
	 * 		<li>M,MM {1-12} para meses;</li>
	 * 		<li>D,DD {1-31} para dias;</li>
	 * 		<li>J,JJ {0-23} para horas;</li>
	 * 		<li>N,NN {0-59} para minutos;</li>
	 * 		<li>S,SS {0-59} para segundos;</li>
	 * 		<li>Q,QQ,QQQ {0-999} para milesegundos.</li>
	 * </ul>
	 * <br/>
	 * Valores não suportados:
	 * <ul>
	 * 		<li>MMM,MMMM {Jan. e Dezembro} para meses;</li>
	 * 		<li>E,EE,EEE,EEEE para dias da semana;</li>
	 * 		<li>A {AM;PM} para indicar AM/PM;</li>
	 * 		<li>H,HH {1-24} para horas com ínicio em 1;</li>
	 * 		<li>K,KK {0-11} para horas com indicador de AM/PM;</li>
	 * 		<li>L,LL {1-12} para horas com indicador de AMP/PM e com ínicio em 1;</li>
	 * </ul>
	 * <br/>
	 * Se o usuário informar uma mascará que tem duas vezes a indicação de ano, por exemplo, os valores
	 * se acumularam. Se em inputFormat tivermos, por exemplo, DD/DD/YYYY os dois DD se juntaram e gerará um número
	 * não suportado.
	 *
	 * Baseado no metodo protegido de DateField parseDateString(str:String);
	 *
	 * @see DateField#parseDateString(str:String)
	 * @param value A <b>String</b> a ser convertida.
	 * @param inputFormat O formato entrada para a conversão. O valor padrão é o Locale.DATE
	 *
	 * @return A <b>Date</b>  obtida na conversão de String.
	 */
	public function toDate(value:String, inputFormat:String = null):Date {
		if (StringUtils.isNullOrEmpty(inputFormat)) {
			inputFormat = "JJ:NN:SS";
		}

		/*
		 * Na String so poderá haver números, não sendo permitido usar
		 * palavras para representar os dias da semana ou os nomes dos mês,
		 * então a mascará deverá ter o mesmo tamanho que o valor a ser
		 * convertido. Para cada formato "A" contido na mascará poderá ter 1
		 * caracter a mais em relação ao valor.
		 */
		var index:int = -1;

		if (inputFormat.length != value.length) {
			return null;
		}

		var yearAsString:String = "";
		var monthAsString:String = "";
		var daysAsString:String = "";
		var hoursAsString:String = "";
		var minutesAsString:String = "";
		var secondsAsString:String = "";
		var msAsString:String = "";

		var temp:String = "";
		for (var i:int = 0; i < inputFormat.length; i++) {
			temp=value.charAt(i);
			switch (inputFormat.charAt(i)) {
				case "Y": /* ANO */  {
					if (!StringUtils.isNullOrEmpty(temp) && !isNaN(Number(temp))) {
						yearAsString+=temp;
					}
					break;
				}
				case "M": /* MÊS */  {
					if (!StringUtils.isNullOrEmpty(temp) && !isNaN(Number(temp))) {
						monthAsString+=temp;
					}
					break;
				}
				case "D": /* DIA */  {
					if (!StringUtils.isNullOrEmpty(temp) && !isNaN(Number(temp))) {
						daysAsString+=temp;
					}
					break;
				}
				case "H": /* HORAS (1..24) */
				case "J": /* HORAS (0..23) */  {
					if (!StringUtils.isNullOrEmpty(temp) && !isNaN(Number(temp))) {
						hoursAsString+=temp;
					}
					break;
				}
				case "N": /* MIN */  {
					if (!StringUtils.isNullOrEmpty(temp) && !isNaN(Number(temp))) {
						minutesAsString+=temp;
					}
					break;
				}
				case "S": /* SEG */  {
					if (!StringUtils.isNullOrEmpty(temp) && !isNaN(Number(temp))) {
						secondsAsString+=temp;
					}
					break;
				}
				case "Q": /* MS */  {
					if (!StringUtils.isNullOrEmpty(temp) && !isNaN(Number(temp))) {
						msAsString+=temp;
					}
					break;
				}
				default:  {
					// Tipo desconhecido
				}
			}
		}

		var year:Number = new Number(yearAsString);
		var month:Number = new Number(monthAsString);
		var days:Number = new Number(daysAsString);
		var hours:Number = new Number(hoursAsString);
		var minutes:Number = new Number(minutesAsString);
		var seconds:Number = new Number(secondsAsString);
		var ms:Number = new Number(msAsString);

		if (isNaN(year) || isNaN(month) || isNaN(days) || isNaN(hours) ||
				isNaN(minutes) || isNaN(seconds) || isNaN(ms)) {

			return null;
		}

		// Se o ano tiver sido inicializado, faz algumas correções
		if (!StringUtils.isNullOrEmpty(yearAsString)) {
			if (yearAsString.length == 2 && year < 70) {
				year += 2000;
			}
		}

		var date:Date = timeZero();
		if (!StringUtils.isNullOrEmpty(yearAsString)) {
			date.setFullYear(year);
		}

		if (!StringUtils.isNullOrEmpty(monthAsString)) {
			date.setMonth(month - 1);
		}

		if (!StringUtils.isNullOrEmpty(daysAsString)) {
			date.setDate(days);
		}

		if (!StringUtils.isNullOrEmpty(hoursAsString)) {
			date.setHours(hours);
		}

		if (!StringUtils.isNullOrEmpty(minutesAsString)) {
			date.setMinutes(minutes);
		}

		if (!StringUtils.isNullOrEmpty(secondsAsString)) {
			date.setSeconds(seconds);
		}

		if (!StringUtils.isNullOrEmpty(msAsString)) {
			date.setMilliseconds(ms);
		}

		return date;
	}
	}
}
