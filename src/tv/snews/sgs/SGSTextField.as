package tv.snews.sgs {

import flash.events.Event;
import flash.text.TextField;

import se.svt.caspar.template.components.CasparTextField;

public class SGSTextField extends CasparTextField {

	private var _textField: flash.text.TextField;

	function SGSTextField(textField:flash.text.TextField, spacing:Number) {
		super(textField, spacing);
		this._textField = textField;
	}

	public override function SetData(xmlData:XML):void {
		{
			super.SetData(xmlData);
			this._textField.dispatchEvent(new Event(Event.CHANGE));
		}
	}
}
}
