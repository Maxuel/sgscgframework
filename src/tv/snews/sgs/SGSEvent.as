/**
 * Created by Maxuel on 9/11/17.
 */
package tv.snews.sgs {
import flash.events.Event;

public class SGSEvent extends Event {

	public static const CHANGE:String = "change";
	private var _entity:Object;

	//----------------------------------------------------------------------
	//	Constructor
	//----------------------------------------------------------------------

	public function SGSEvent(type:String, entity:Object = null, bubbles:Boolean = true, cancelable:Boolean = false) {
		super(type, bubbles, cancelable);
		_entity = entity;
	}

	//----------------------------------------------------------------------
	//	Properties
	//----------------------------------------------------------------------

	public function get entity():Object {
		return _entity;
	}

	public function set entity(value:Object):void {
		_entity = value;
	}
}
}
