package tv.snews.sgs {

import flash.display.DisplayObject;
import flash.display.MovieClip;
import flash.events.Event;
import flash.events.TimerEvent;
import flash.text.TextField;
import flash.utils.Timer;

import se.svt.caspar.template.CasparTemplate;

import tv.snews.util.Crawler;
import tv.snews.util.RSSFeed;
import tv.snews.util.DateUtil;
import tv.snews.util.Roll;
import tv.snews.util.StringUtils;
import tv.snews.util.Swap;

public class SGSTemplate extends CasparTemplate {
	function SGSTemplate() {
		super();
	}

	private var dateUtil:DateUtil = new DateUtil();
	private var _images:Array = new Array();

	override public function SetData(xmlData:XML):void {
		for each (var element:XML in xmlData.children()) {
			for each(var image:Object in _images) {
				if (element.@id == image.name) {
					image.source = element.data.@value.toString();
				}
			}
		}
		super.SetData(xmlData);
	}

	public function sgsSwap(movie:MovieClip, tf:TextField):void {
		var swap:Swap = new Swap(movie, tf, this);
		swap.start();
	}

	public function sgsClock(tf:TextField, format:String = "JJ:NN", timezone:TextField = null ):void {
		setTime(tf, format, timezone);
		var timer:Timer = new Timer(490);
		timer.addEventListener(TimerEvent.TIMER, function (ev:TimerEvent):void {
            setTime(tf, format, timezone);
		});
		timer.start();
		"".toLowerCase()
	}


	public function sgsRSSCrawl(speed:TextField, bounds:DisplayObject,rssUrl:TextField, tf:TextField = null, interval:TextField = null, separator:TextField = null):void{
        var rssFeed:RSSFeed = new RSSFeed(this, bounds, tf, rssUrl, speed, interval, separator);
        rssFeed.start();
        addChild(rssFeed);
	}


	public function sgsCrawl(speed:TextField, bounds:DisplayObject, tf:TextField = null):void {
		var crawler:Crawler = new Crawler(this, speed, bounds, tf);
		crawler.start();
		addChild(crawler);
	}

	public function sgsRoll(speed:Object, spacing:Number, stopAtLastItem:Boolean, bounds:DisplayObject, ...elements):void {
		var roll:Roll = new Roll(this, speed, spacing, stopAtLastItem, bounds, elements);
		roll.start();
		addChild(roll);
	}


	public function sgsCountdown(tf:TextField, format:String, interval:String, untilZero:Boolean = true):void {
		var target:Date;
		var startTime:Date = new Date();

		var timer:Timer = new Timer(getInterval(interval));
		timer.addEventListener(TimerEvent.TIMER, function (ev:TimerEvent):void {
			if (!target)
				target = dateUtil.toDate(StringUtils.trim(tf.text), format);
			var now:Date = new Date();
			var timeElapsed:Date = dateUtil.subtractTimes(now, startTime);

			if (timeElapsed > target) {
				if(!untilZero)
					tf.text = "-" + dateUtil.format(dateUtil.subtractTimes(timeElapsed, target), format);
				else
					timer.stop();

			} else {
				tf.text = dateUtil.format(dateUtil.subtractTimes(timeElapsed, target), format);
			}
		});
		timer.start();
	}

	public function sgsTimer(tf:TextField, format:String, interval:String):void {
		var target:Date;
		var startTime:Date = new Date();

		var timer:Timer = new Timer(getInterval(interval));
		timer.addEventListener(TimerEvent.TIMER, function (ev:TimerEvent):void {
			if (!target)
				target = dateUtil.toDate(StringUtils.trim(tf.text), format);
			var now:Date = new Date();
			var timeElapsed:Date = dateUtil.subtractTimes(now, startTime);

			tf.text = dateUtil.format(dateUtil.addTimes(timeElapsed, target), format);
		});
		timer.start();
	}


	public function get images():Array {
		return _images;
	}

	public function set images(value:Array):void {
		_images = value;
	}

	//---------------------------
	//  Helper's
	//---------------------------

	private function setTime(tf:TextField, format:String, timezone:TextField):void {
		var date:Date = new Date();
		var timezoneNum = timezone ? (date.getTimezoneOffset() / 60) + Number(timezone.text) : 0;

		date = dateUtil.sumHours(date, timezoneNum);

		tf.text = dateUtil.format(date, format);
	}

	private function twoDigitsNumber(value:Number):String {
		if (value < 10) {
			return "0" + value;
		}
		return "" + value;
	}

	private function getInterval(intervalStr:String):Number {
		switch (intervalStr) {
			case "ms":
				return 1;
			case "s":
				return 900;
			case "m":
				return 5900;
			case "h":
				return 360000;
			default:
				return 1000;
		}
	}

}

}